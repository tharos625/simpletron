// Simpletron - Lenguaje de Maquina Simpletron (LMS)
// Como Programar en C/C++ - Cap. 7. Apuntadores - pag. 305

#include <stdio.h>
#define READ 10
#define WRITE 11

#define LOAD 20
#define STORE 21

#define ADD 30
#define SUBTRACT 31
#define DIVIDE 32
#define MULTIPLY 33

#define BRANCH 40
#define BRANCHNEG 41
#define BRANCHZERO 42
#define HALT 43

//prototipos de funciones
void pantalla_inicio();
void vaciado_completo();

int main()
{
    int memory[100] = {0};

    int accumulator = 0;
    int instructionCounter = 0;
    int instructionRegister = 0;
    int operationCode = 0;
    int operand = 0;

    pantalla_inicio();

    do 
    {
        printf("%02d ? ", instructionCounter);
        scanf("%d", &memory[instructionCounter]);

        if (memory[instructionCounter] == -99999 )
        {
            memory[instructionCounter] = 0;
            puts("\n*** Program loading completed ***");
            puts("*** Program execution begins ***\n");
            break;
        }

        if ((memory[instructionCounter] >= -9999) && (memory[instructionCounter] <= 9999))
            instructionCounter++;

    } while(1);

    instructionCounter = 0;

    do
    {
        instructionRegister = memory[instructionCounter];
        operationCode = instructionRegister / 100;
        operand = instructionRegister % 100;



        switch (operationCode)
        {
            case READ:
                printf("? ");
                scanf("%d", &memory[operand]);
                ++instructionCounter;
                break;

            case WRITE:
                puts("\n****************");
                printf("* OUTPUT: %d\n", memory[operand]);
                puts("****************\n");
                ++instructionCounter;
                break;

            case LOAD:
                accumulator = memory[operand];
                ++instructionCounter;
                break;

            case STORE:
                memory[operand] = accumulator;
                ++instructionCounter;
                break;

            case ADD:
                accumulator += memory[operand];
                if ((accumulator < -9999) || (accumulator > 9999))
                {
                    puts("*** Accumulator Overload ***");
                    puts("*** Simpletron execution abnormaly terminated ***\n");
                    operationCode = HALT;
                    break;
                }
                ++instructionCounter;
                break;

            case SUBTRACT:
                accumulator -= memory[operand];
                if ((accumulator < -9999) || (accumulator > 9999))
                {
                    puts("*** Accumulator Overload ***");
                    puts("*** Simpletron execution abnormaly terminated ***\n");
                    operationCode = HALT;
                    break;
                }
                ++instructionCounter;
                break;

            case DIVIDE:
                if ((accumulator == 0) && (memory[operand]) == 0)
                {
                    puts("*** Attempt to divide by zero ***");
                    puts("*** Simpletron execution abnormaly terminated ***\n");
                    operationCode = HALT;
                    break;
                }
                accumulator /= memory[operand];
                ++instructionCounter;
                break;

            case MULTIPLY:
                accumulator *= memory[operand];
                if ((accumulator < -9999) || (accumulator > 9999))
                {
                    puts("*** Accumulator Overload ***");
                    puts("*** Simpletron execution abnormaly terminated ***\n");
                    operationCode = HALT;
                    break;
                }
                ++instructionCounter;
                break;

            case BRANCH:
                instructionCounter = operand;
                break;

            case BRANCHNEG:
                if (accumulator < 0)
                    instructionCounter = operand;
                break;

            case BRANCHZERO:
                if (accumulator == 0)
                    instructionCounter = operand;
                break;

            case HALT:
                puts("*** Simpletron execution terminated ***");
                break;
            
            default:
                puts("*** Operation Code Invalid ***");
                puts("*** Simpletron execution abnormaly terminated ***\n");
                operationCode = HALT;
                break;

        }
    } while (operationCode != HALT);

    vaciado_completo(memory, accumulator, instructionCounter, instructionRegister, operationCode, operand);

    return 0;
}


void pantalla_inicio()
{
    printf("*** Welcome to Simpletron! ***\n\n");
    printf("*** Please enter your program one instruction ***\n");
    printf("*** (or data word) at a time. I will type the ***\n");
    printf("*** location number and a question mark (?). ***\n");
    printf("*** You then type the word for that location. ***\n");
    printf("*** Type the sentinel -99999 to stop entering ***\n");
    printf("*** your program. ***\n\n");
}

void vaciado_completo(int mem[], int accu, int instCounter, int instReg, int opCode, int op)
{
    puts("REGISTERS:");
    printf("accumulator%+15.4d\n", accu);
    printf("instruccionCounter%8.2d\n", instCounter);
    printf("instruccionRegister%+7.4d\n", instReg);
    printf("operationCode%13.2d\n", opCode);
    printf("operand%19.2d\n\n", op);

    puts("MEMORY:");
    puts("       0     1     2     3     4     5     6     7     8     9");
    int x = 0;
    for ( x = 0; x <= 99; x++)
    {
        int y = 1;
        printf("%2d ", x);
        for (y = 1; y <= 10; y++)
        {
            printf("%+05d ", mem[x]);
            x++;
        }
        x--;
        puts("");
    }
    puts("");
}
